/**
 * gulp環境変数
 *
 * @html: HTMLファイルの格納ディレクトリ
 * @css:
 *  - src: SCSSファイルの格納先
 *  - dest: CSSファイルの格納先
 *  - supportBrowser: サポートブラウザ
 * @js:
 *  - src: 開発用JSファイルの格納先
 *  - dest: JSファイルの格納先
 * @imagemin:
 *  - path: 画像の格納先
 * @server: ローカルサーバーのルートディレクトリ
 */
module.exports.setting = {
	html: {
		src: 'dev/ejs/',
		dest: 'root/',
		vdest: 'validate/'
	},
	css: {
		minify: false,
		src: 'dev/scss/',
		dest: 'root/assets/css/'
	},
	js: {
		src: 'dev/js/',
		dest: 'root/assets/js/'
	},
	imagemin: {
		path: 'root/assets/img/',
		quality: '80-90'
	},
	server: {
		base: 'root',
		watch: './root'
	}
};


/**
 * ロードモジュールの設定
 */
module.exports.loadPlugins = {
	pattern: ['gulp-*', 'gulp.*', 'browser-sync', 'run-sequence', 'imagemin-pngquant', 'imagemin-jpegtran', 'node-sass-package-importer'],
	rename: {
		'browser-sync': 'browserSync',
		'run-sequence': 'sequence',
		'imagemin-pngquant': 'pngquant',
		'imagemin-jpegtran': 'jpegtran',
		'gulp-connect-php': 'php',
		'gulp-clean-css': 'cleanCSS',
		'node-sass-package-importer': 'sassImporter'
	}
};
